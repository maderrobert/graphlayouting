package layout;


import java.awt.geom.Point2D;
import java.awt.Rectangle;
import java.util.Set;

import guiGraph.ConcurrentCoordinateEdge;
import guiGraph.ConcurrentCoordinateNode;
import guiGraph.ConcurrentGraphWrapper;

/**
 * A force-directed layouter
 * 
 * @author Robert Mader
 * 
 */

class ForceDirectedLayouter {
	
	private final double linkStrengh = .01;	// [0,1] Strengh of links between vertices
	private double linkDistance = 0;	// Targeted link distance
	private final double charge = -1;		// Negative for node repulsion
	private final double gravity = 0.1;		// "gravity" to the center of the layout. Similar to linkStrengh
	private final double theta = 0.8;			// Barnes–Hut approximation criterion
	
	private boolean converged = false;
	private final double stepOrig = .05;
	private double step = stepOrig;
	private double energy = Double.MAX_VALUE;
	private final double t = 0.9;
	private int progress = 0;
	private double smoothGoal = 0.1;
	
	//variables for classical calculation of link attraction and charge
	private final double c_3 = 50000;
	
	//dimensions of layout, to determine the center
	private Point2D.Double center;
	private double factor;
	
	private ConcurrentGraphWrapper graph;
	
	/**
	 * Initialize the layouter
	 * @param graph
	 * @param width
	 * @param height
	 */
	ForceDirectedLayouter(ConcurrentGraphWrapper graph){
		this.graph = graph;
		Rectangle rec = graph.getBounds();
		center = new Point2D.Double(rec.x+rec.width/2,rec.y+rec.height/2);
		
		this.factor = Math.min(rec.width, rec.width);
		linkDistance *= factor;
		step *= factor;
	}
	
	/**
	 * This executes one iteration of the algorithm
	 */
	public void run(){
		
		if(graph.getResetLayouter()){
			graph.setResetLayouter(false);
			reset();
		}
		else if(converged) return;
		
		Set<ConcurrentCoordinateNode> nodes = graph.getNodes();
		Set<ConcurrentCoordinateEdge> edges = graph.getEdges();
		
		if(nodes.size() == 0) return;
		
		for(ConcurrentCoordinateNode n : nodes){
			n.resetTemporaryCoordinates();
		}
		
		/* 
		 * Forces between adjacent vertices
		 */
		linkAttraction(nodes,edges);
		
		/*
		 * Gravity to the center of the layout
		 */
		gravity(nodes);
		
		/*
		 * charge/repulsive force - naive and to be replaced by sophisticated algorithm
		 */
		//chargeNaive(nodes);
		
		/*
		 * charge/repulsive force - using Barnes-Hut-Trees to calculate forces more efficiently/scalable
		 */
		chargeBarnesHut(nodes);
		
		/*
		 * Smooth movements, try cooling the system
		 */
		smoothMovement(nodes);
		
		//check for bugs - to be removed
		/*for(ConcurrentCoordinateNode n : nodes){
			if(Double.isInfinite(n.getTemporaryCoordinates().x) || Double.isNaN(n.getTemporaryCoordinates().x) || Double.isInfinite(n.getTemporaryCoordinates().y) || Double.isNaN(n.getTemporaryCoordinates().y)){
				System.out.println("----------------------------after smoothing-----------------------------------");
				System.out.println("n.getCoordinates(): " + n.getCoordinates().x + "," + n.getCoordinates().y);
				System.out.println("n.getTemporaryCoordinates().x: " + n.getTemporaryCoordinates().x + "," + n.getTemporaryCoordinates().y);
				System.exit(1);
			}
		}*/
		
		/*
		 * Apply combined forces to original graph. As we don't know if we get copies of original notes,
		 * set values explicitly
		 */
		for(ConcurrentCoordinateNode n : nodes){	
			n.makeTemporaryCoordinatesPermanent();
		}
		
		graph.repaint();
	}
	
	/**
	 * Forces between adjacent vertices
	 * @param nodes
	 * @param edges
	 */
	private void linkAttraction(Set<ConcurrentCoordinateNode> nodes, Set<ConcurrentCoordinateEdge> edges){
		
		
		
		for(ConcurrentCoordinateEdge e : edges){
			
			ConcurrentCoordinateNode n1 = e.getHead();
			ConcurrentCoordinateNode n2 = e.getTail();
			Point2D.Double coorN1 = n1.getCoordinates();
			Point2D.Double coorN2 = n2.getCoordinates();
			
			//calculate distance between vertices, with target distance linkDistance
			double xLen = coorN2.x - coorN1.x;
			double yLen = coorN2.y - coorN1.y;
			double forceStrength = coorN1.distance(coorN2) - linkDistance;
			
			//calculate force as mentioned in Handbook of Graph Drawing and Visualization,page 385, logarithmic
			//if(forceStrength > 0) forceStrength = c_1 * Math.log(forceStrength/c_2);
			
			//Use simple/linear method instead
			forceStrength *= linkStrengh;
			
			//calculate angle of force
			double angle = Math.atan2(yLen, xLen);
			
			/*apply force dependent on angle on new coordinates.
			 * To make all calculations of forces independent from each other,
			 * calculate on the basis of original coordinates and apply to new coordinates
			*/
			n1.addToTempCoordinates(Math.cos(angle) * forceStrength, Math.sin(angle) * forceStrength);
			
			//invert angle in radians: +/- pi
			angle += Math.PI;
			n2.addToTempCoordinates(Math.cos(angle) * forceStrength, Math.sin(angle) * forceStrength);
		}
	}
	
	/**
	 * Gravity to the center of the layout
	 * @param nodes
	 */
	private void gravity(Set<ConcurrentCoordinateNode> nodes){
		for(ConcurrentCoordinateNode n : nodes){
			
			Point2D.Double coor = n.getCoordinates();
			
			//calculate distance between vertex and center
			double xLen = center.x - coor.x;
			double yLen = center.y - coor.y;
			double forceStrength = coor.distance(center);
			
			//apply linear gravity factor
			forceStrength *= gravity;
			
			//calculate angle of force
			double angle = Math.atan2(yLen, xLen);
			
			/*apply force dependent on angle on new coordinates.
			 * To make all calculations of forces independent from each other,
			 * calculate on the basis of original coordinates and apply to new coordinates
			*/
			n.addToTempCoordinates(Math.cos(angle) * forceStrength, Math.sin(angle) * forceStrength);
		}
	}
	
	/**
	 * charge/repulsive force - naive and to be replaced by sophisticated algorithm
	 * @param nodes
	 */
	@SuppressWarnings("unused")
	private void chargeNaive(Set<ConcurrentCoordinateNode> nodes){
		for(ConcurrentCoordinateNode n1 : nodes){
			for(ConcurrentCoordinateNode n2 : nodes){
				try{
					if(n1 == n2 || graph.areAdjacent(n1, n2))
						continue;
					
					Point2D.Double coor1 = n1.getCoordinates();
					Point2D.Double coor2 = n2.getCoordinates();
					
					//calculate distance between vertices
					double xLen = coor2.x - coor1.x;
					double yLen = coor2.y - coor1.y;
					double forceStrength = coor1.distance(coor2);
					
					//calculate force as mentioned in Handbook of Graph Drawing and Visualization,page 385, logarithmic
					if(forceStrength > 0)
						forceStrength = c_3/Math.pow(forceStrength, 2);
					
					//calculate angle of force
					double angle = Math.atan2(yLen, xLen);
					
					//invert as the force has to go in the opposite direction
					angle += Math.PI;
					
					/*apply force dependent on angle on new coordinates.
					 * To make all calculations of forces independent from each other,
					 * calculate on the basis of original coordinates and apply to new coordinates
					*/
					n1.addToTempCoordinates(Math.cos(angle) * forceStrength, Math.sin(angle) * forceStrength);
	
					//invert angle in radians: +/- pi
					angle += Math.PI;
					n2.addToTempCoordinates(Math.cos(angle) * forceStrength, Math.sin(angle) * forceStrength);
				}
				catch(IllegalArgumentException e){
					continue;
				}
			}
		}
	}
	
	/**
	 * charge/repulsive force - using Barnes-Hut-Trees to calculate forces more efficiently/scalable
	 * @param nodes
	 */
	private void chargeBarnesHut(Set<ConcurrentCoordinateNode> nodes){
		
		//calc min/max for area
		
		double minX = Double.POSITIVE_INFINITY;
		double minY = Double.POSITIVE_INFINITY;
		double maxX = Double.NEGATIVE_INFINITY;
		double maxY = Double.NEGATIVE_INFINITY;
		
		for(ConcurrentCoordinateNode n : nodes){
			if(n.getCoordinates().x < minX)
				minX = n.getCoordinates().x;
			if(n.getCoordinates().x > maxX)
				maxX = n.getCoordinates().x;
			if(n.getCoordinates().y < minY)
				minY = n.getCoordinates().y;
			if(n.getCoordinates().y > maxY)
				maxY = n.getCoordinates().y;
		}
		
		//create tree
		BarnesHutNode bhTree = new BarnesHutNode(graph, new Point2D.Double(minX,minY), Double.max(maxX-minX, maxY-minY));
		for(ConcurrentCoordinateNode n : nodes){
			if(!n.getIgnoredByLayouter()) bhTree.addNode(n);
		}
		
		//calculate force on every node
		for(ConcurrentCoordinateNode n : nodes){
			if(!n.getIgnoredByLayouter()) addBHForce(bhTree, n);
		}
	}
	
	/**
	 * 
	 * @param bhNode
	 * @param node
	 */
	private void addBHForce(BarnesHutNode bhNode, ConcurrentCoordinateNode node){
		if(bhNode.isEmpty()){
			return;
		}
		else if(bhNode.isExternal() && bhNode.getBody().getIdentity() != node.getIdentity()){
			addChargeForce(bhNode, node);
		}
		else if(bhNode.getSize()/node.getCoordinates().distance(bhNode.getCenterOfMass()) < theta){
			addChargeForce(bhNode, node);
		}
		else{
			for(BarnesHutNode bhn : bhNode.getChilds()){
				addBHForce(bhn, node);
			}
		}
	}
	
	/**
	 * 
	 * @param bhNode
	 * @param node
	 */
	private void addChargeForce(BarnesHutNode bhNode, ConcurrentCoordinateNode node){
		
		Point2D.Double coorBHN = bhNode.getCenterOfMass();
		Point2D.Double coorNode = node.getCoordinates();
		
		//calculate distance between vertices
		double xLen = coorBHN.x - coorNode.x;
		double yLen = coorBHN.y - coorNode.y;
		double forceStrength = coorBHN.distance(coorNode);
		
		//calculate force as mentioned in Handbook of Graph Drawing and Visualization,page 385, logarithmic
		if(forceStrength == 0) forceStrength = 0;
		else forceStrength = c_3/Math.pow(forceStrength, 2);
		
		//consider mass proportion
		forceStrength *= bhNode.getMass();
		/*double forceStrengthNew = forceStrength;
		try{
			forceStrengthNew *= bhNode.getMass()/(graph.adjacentTo(node).size()+1);
		}
		finally{
			if(Double.isInfinite(forceStrengthNew) || Double.isNaN(forceStrengthNew)){
				forceStrengthNew = forceStrength * bhNode.getMass();
			}
		}
		forceStrength = Math.log(forceStrengthNew);*/
		
		//apply charge factor
		forceStrength *= charge;
		
		//calculate angle of force
		double angle = Math.atan2(yLen, xLen);
		
		/*apply force dependent on angle on new coordinates.
		 * To make all calculations of forces independent from each other,
		 * calculate on the basis of original coordinates and apply to new coordinates
		*/
		node.addToTempCoordinates(Math.cos(angle) * forceStrength, Math.sin(angle) * forceStrength);

	}
	
	/**
	 * 
	 * @param nodes
	 */
	private void smoothMovement(Set<ConcurrentCoordinateNode> nodes){
		double energy_old = energy;
		energy = 0;
		double sum = 0;
		
		for(ConcurrentCoordinateNode n : nodes){
			
			Point2D.Double coor = n.getCoordinates();
			Point2D.Double tempCoor = n.getTemporaryCoordinates();

			double xLen = tempCoor.x - coor.x;
			double yLen = tempCoor.y - coor.y;
			
			//adaptive cooling scheme
			double dist = coor.distance(tempCoor);
			energy += Math.pow(dist, 2);
			
			double forceStrength = Math.min(step, dist);
			double angle = Math.atan2(yLen, xLen);
			
			n.setTemporaryCoordinates(coor.x + Math.cos(angle) * forceStrength, coor.y + Math.sin(angle) * forceStrength);
		
			sum += coor.distance(tempCoor);
		}
		
		update_steplength(energy_old);
		
		if(sum < smoothGoal * Math.log(nodes.size())){
			converged = true;
		}
		
	}

	private void update_steplength(double energy_old) {
		if(energy < energy_old){
			progress += 1;
			if(progress >= 5){
				progress = 0;
				step /= t;
			}
		}
		else{
			progress = 0;
			step *= t;
		}
	}
	
	private void reset(){
		step = stepOrig * factor;
		energy = Double.MAX_VALUE;
		progress = 0;
		converged = false;
	}
}
