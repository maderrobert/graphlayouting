package layout;

import java.awt.geom.Point2D.Double;
import java.util.Set;

import de.fu_berlin.mi.graph.BaseGraph;
import guiGraph.ConcurrentCoordinateEdge;
import guiGraph.ConcurrentCoordinateNode;

/**
 * 
 * @author Robert Mader
 *
 */
class GridLayouter {
	/**
	 * Creates a grid layout
	 * @param graph
	 * @param width
	 * @param height
	 */
	static void layout(BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge> graph, double minX, double minY, double width, double height){

		/*System.out.println("----GridLayouter----");
		System.out.print("minX: ");
		System.out.println(minX);
		System.out.print("minY: ");
		System.out.println(minY);
		System.out.print("maxX: ");
		System.out.println(width + minX);
		System.out.print("maxY: ");
		System.out.println(height + minY);
		/*System.out.print("width: ");
		System.out.println(width);
		System.out.print("height: ");
		System.out.println(height);*/
		
		double sideLength = Math.sqrt(graph.getNodeCount());
		/*System.out.print("sideLength: ");
		System.out.println(sideLength);*/
		double scaleX = width / sideLength;
		/*System.out.print("scaleX: ");
		System.out.println(scaleX);*/
		double scaleY = height / sideLength;
		/*System.out.print("scaleY: ");
		System.out.println(scaleY);*/
		
		
		Set<ConcurrentCoordinateNode> nodes = graph.getNodes();
		
		double currentX = 0;
		double currentY = 0;
		
		for(ConcurrentCoordinateNode n : nodes){
			graph.getNode(n.getIdentity()).setCoordinates(new Double(currentX+minX, currentY+minY));
			/*System.out.print("coordinate: ");
			System.out.print(currentX+minX);
			System.out.print(" / ");
			System.out.println(currentY+minY);*/
			
			currentX += scaleX;
			if(currentX>sideLength*scaleX){
				currentX = 0;
				currentY += scaleY;
			}
		}
		
		//System.out.println("--------");
	}
}
