package layout;

import guiGraph.ConcurrentGraphWrapper;

/**
 * Thread to layout a given graph in the background
 * 
 * usage:
 * 
 * LayoutThread thread = new LayoutThread(graph, width, height, 30);
 * thread.start();
 * ...
 * thread.cancel();
 * thread.join();
 * 
 * @author Robert Mader
 * 
 */
class LayoutThread extends Thread{
	ForceDirectedLayouter layouter;
	int wait;
	boolean interrupted = false;
	
	/**
	 * Create thread
	 * @param graph
	 * @param width
	 * @param height
	 * @param maxFPS
	 */
	LayoutThread(ConcurrentGraphWrapper graph, int maxFPS){
		this.layouter = new ForceDirectedLayouter(graph);
		if(maxFPS > 0){
			this.wait = 1000/maxFPS;
		}
		else{
			this.wait = 0;
		}
	}
	
	/**
	 * Execute the layouter
	 */
	public void run(){
		try{
			while (!Thread.currentThread().isInterrupted()){
				long startTime = System.currentTimeMillis();
				layouter.run();
				long elapsedTime = System.currentTimeMillis() - startTime;
				if(elapsedTime < wait)
					Thread.sleep(wait-elapsedTime);
			}
		}
		catch (InterruptedException consumed) {
		}
	}
	
	/**
	 * Use this before a join to stop the thread
	 */
	public void cancel() { interrupt(); }
}
