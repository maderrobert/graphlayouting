package layout;

import java.util.HashSet;
import java.util.Set;

import de.fu_berlin.mi.graph.BaseGraph;
import guiGraph.ConcurrentCoordinateEdge;
import guiGraph.ConcurrentCoordinateNode;

/**
 * 
 * Tree to split area evenly for graph components
 * 
 * @author Robert Mader
 *
 */
public class LayoutSplitNode {
	private BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge> graphComponent = null;
	private long weight = 0;
	private final double minX;
	private final double width;
	private final double minY;
	private final double height;
	private final boolean toggle;
	
	boolean hasChilds = false;
	LayoutSplitNode left;
	LayoutSplitNode right;
	
	LayoutSplitNode(){
		this(0,0,1,1,false);
	}

	LayoutSplitNode(double minX, double minY, double width, double height, boolean toggle){
		this.minX = minX;
		this.minY = minY;
		this.width = width;
		this.height = height;
		this.toggle = toggle;
	}
	
	/**
	 * Add component to tree.
	 * If this node is empty, add the component to it
	 * If node is non empty leaf, create child nodes
	 * else add component to less heavy child
	 * 
	 * @param graphComponent
	 */
	public LayoutSplitNode addComponent(BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge> graphComponent){
		if(this.graphComponent == null && !hasChilds){
			this.graphComponent = graphComponent;
			this.weight = graphComponent.getNodeCount();
			return this;
		}
		else if(!hasChilds){
			initializeChilds();
			this.weight += graphComponent.getNodeCount();
			return this.addComponent(graphComponent);
		}
		else{
			this.weight += graphComponent.getNodeCount();
			return (left.getWeight() <= right.getWeight() ? left : right).addComponent(graphComponent);	
		}
	}

	/**
	 * Initialize children
	 */
	private void initializeChilds() {
		if(hasChilds) return;
		
		if(toggle){
			left = new LayoutSplitNode(minX, minY, width, height/2, !toggle);
			right = new LayoutSplitNode(minX, minY + height/2, width, height/2, !toggle);
		}
		else{
			left = new LayoutSplitNode(minX, minY, width/2, height, !toggle);
			right = new LayoutSplitNode(minX + width/2, minY, width/2, height, !toggle);
		}
		hasChilds = true;
		
		if(graphComponent != null){
			BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge> temp = graphComponent;
			graphComponent = null;
			left.addComponent(temp);
		}
	}
	
	/**
	 * Return weight/node count of subtree
	 */
	public long getWeight(){
		return weight;
	}
	
	/**
	 * Get all leafs
	 */
	public Set<LayoutSplitNode> getLeafs(){
		if(hasChilds){
			Set<LayoutSplitNode> res = left.getLeafs();
			res.addAll(right.getLeafs());
			return res;
		}
		else if(graphComponent != null){
			Set<LayoutSplitNode> res = new HashSet<LayoutSplitNode>();
			res.add(this);
			return res;
		}
		return new HashSet<LayoutSplitNode>();
	}
	
	public BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge> getGraphComponent(){
		return graphComponent;
	}
	
	public double getMinX(){
		return minX;
	}
	
	public double getMinY(){
		return minY;
	}
	
	public double getWidth(){
		return width;
	}
	
	public double getHeight(){
		return height;
	}
}
