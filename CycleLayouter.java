package layout;

import java.util.Set;
import java.util.Vector;

import de.fu_berlin.mi.graph.BaseGraph;
import guiGraph.ConcurrentCoordinateEdge;
import guiGraph.ConcurrentCoordinateNode;

/**
 * 
 * @author Robert Mader
 *
 */
class CycleLayouter {
	/**
	 * Creates a grid layout
	 * @param graph
	 * @param width
	 * @param height
	 */
	static void layout(BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge> graph, double minX, double minY, double width, double height){
		
		double centerX = minX + width/2;
		double centerY = minY + height/2;
		double radius = Math.min(width, height) / 2;
		double angleStep = Math.PI*2/graph.getNodeCount();
		double angle = 0;
		
		Set<ConcurrentCoordinateNode> nodes = graph.getNodes();
		
		for(ConcurrentCoordinateNode n : nodes){
			n.setCoordinates(centerX + Math.cos(angle) * radius, centerY + Math.sin(angle) * radius);
			angle += angleStep;
		}
	}
	
	static void layout(Vector<ConcurrentCoordinateNode> nodes, double minX, double minY, double width, double height){
		
		double centerX = minX + width/2;
		double centerY = minY + height/2;
		double radius = Math.min(width, height) / 2;
		double angleStep = Math.PI*2/nodes.size();
		double angle = 0;
		
		for(ConcurrentCoordinateNode n : nodes){
			n.setCoordinates(centerX + Math.cos(angle) * radius, centerY + Math.sin(angle) * radius);
			angle += angleStep;
		}
	}
}
