package layout;

import java.util.Timer;
import java.util.TimerTask;

import de.fu_berlin.mi.graph.factories.GraphFactory;
import guiGraph.ConcurrentGraphWrapper;

/**
 * Layout manager object
 * All communication with the layouter should be done over this class.
 * 
 * @author Robert Mader
 *
 */
@SuppressWarnings("rawtypes")
public class LayoutManager {
	ConcurrentGraphWrapper graph;
	GraphFactory graphFactory;
	LayoutThread thread;
	TimerTask task;
	Timer timer;
	
	double minX = 0;
	double minY = 0;
	double width = 800;
	double height = 540;
	int fps = 25;
	
	boolean layoutThreadRunning = false;
	
	
	public LayoutManager(){
		this(null, null);
	}
	/**
	 * 
	 * @param graph
	 * @param graphFactory
	 */
	public LayoutManager(ConcurrentGraphWrapper graph, GraphFactory graphFactory){
		this.graph = graph;
		this.graphFactory = graphFactory;
	}
	
	/**
	 * 
	 * @param graph
	 * @param graphFactory
	 */
	public void setGraph(ConcurrentGraphWrapper graph, GraphFactory graphFactory){
		if(layoutThreadRunning){
			endLayoutThread();
			this.graph = graph;
			this.graphFactory = graphFactory;
			startLayoutThread();
		}
		else{
			this.graph = graph;
			this.graphFactory = graphFactory;
		}
	}
	
	public void setDimensions(double minX, double minY, double width, double height){
		if(layoutThreadRunning){
			endLayoutThread();
			this.minX = minX;
			this.minY = minY;
			this.width = width;
			this.height = height;
			startLayoutThread();
		}
		else{
			this.width = width;
			this.height = height;
		}
	}
	
	/**
	 * Create complete new layout for the graph
	 */
	public void rebuildLayout(){
		if(graph == null || graphFactory == null) return;
		
		if(layoutThreadRunning){
			endLayoutThread();
			InitialLayouter.initializeLayout(graph, graphFactory, minX, minY, width, height);
			startLayoutThread();
		}
		else{
			InitialLayouter.initializeLayout(graph, graphFactory, minX, minY, width, height);
		}
	}
	
	/**
	 * Start background layouting
	 */
	public void startLayoutThread(){
		if(layoutThreadRunning) return;
		else if(graph != null){
			graph.setResetLayouter(false);
			timer = new Timer();
			TimerTask task = new LayoutTask(graph);
			timer.scheduleAtFixedRate(task, 0, 1000/fps);
			
			/*thread = new LayoutThread(graph, width, height, fps);
			thread.start();*/
		}
		layoutThreadRunning = true;
	}
	
	/**
	 * Stop background layouting
	 */
	public void endLayoutThread(){
		if(!layoutThreadRunning) return;
		
		if(timer != null){
			timer.cancel();
		}
			
		
		/*if(thread != null){
			thread.cancel();
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}*/
		layoutThreadRunning = false;
	}
	
	public void setLayoutThreadFPS(int fps){
		if(layoutThreadRunning){
			endLayoutThread();
			this.fps = fps;
			startLayoutThread();
		}
		else{
			this.fps = fps;
		}
	}
}
