package layout;

import java.util.TimerTask;

import guiGraph.ConcurrentGraphWrapper;

/**
 * Task to layout a given graph in the background
 * 
 * @author Robert Mader
 * 
 */
class LayoutTask extends TimerTask{
	ForceDirectedLayouter layouter;
	
	/**
	 * Create thread
	 * @param graph
	 * @param width
	 * @param height
	 */
	LayoutTask(ConcurrentGraphWrapper graph){
		layouter = new ForceDirectedLayouter(graph);
	}
	
	/**
	 * Execute the layouter
	 */
	public void run(){
		layouter.run();
	}
}
