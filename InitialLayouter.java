package layout;

import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import de.fu_berlin.mi.graph.BaseGraph;
import de.fu_berlin.mi.graph.factories.GraphFactory;
import guiGraph.ConcurrentCoordinateEdge;
import guiGraph.ConcurrentCoordinateNode;
import guiGraph.ConcurrentGraphWrapper;

/**
 * Create a complete new layout for a graph
 * 
 * @author Robert Mader
 *
 */

@SuppressWarnings("rawtypes")
class InitialLayouter {
	static final double distance = 50;	//not yet used
	static final double padding = 0.1;	//try to have this padding to border
	
	/**
	 * Create a layout from ground up
	 * 
	 * @param graph
	 * @param graphFactory 	(needed for Datastructures...)
	 * @param minX			start coordinate
	 * @param minY			start coordinate
	 * @param width
	 * @param height
	 */
	@SuppressWarnings("unchecked")
	static void initializeLayout(ConcurrentGraphWrapper graph, GraphFactory graphFactory, double minX, double minY, double width, double height){
		
		/*
		 * Split into components
		 */
		
		Set<BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge>> graphSet;
		try{
			graphSet = graph.getConnectedComponents(graphFactory);
		}
		catch(IllegalArgumentException e){
			System.out.println("Some bug happened in graph.getConnectedComponents()");
			System.out.println("Please report to the respective datastructure project");
			return;
		}
			
		System.out.print("Graph components: ");
		System.out.println(graphSet.size());
		
		/*
		 * Split layout so components get spread evenly
		 */
		LayoutSplitNode layoutSplit = new LayoutSplitNode(minX, minY, width, height, false);
		for(BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge> G : graphSet){
			layoutSplit.addComponent(G);
		}
		
		/*
		 * Check for planetary embedding
		 */
		for(LayoutSplitNode l : layoutSplit.getLeafs()){
			
			//convenience variables
			BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge> g = l.getGraphComponent();
			
			if(g.getNodeCount() <= 3){	//always planar and easy
				if(g.getNodeCount() == 1){
					g.getNodes().iterator().next().setCoordinates(l.getMinX()+l.getWidth()/2, l.getMinY()+l.getHeight()/2);
				}
				else{
					CycleLayouter.layout(	g, 
											l.getMinX()+padding*(l.getWidth()), 
											l.getMinY()+padding*(l.getHeight()), 
											l.getWidth()-padding*(l.getWidth()), 
											l.getHeight()-padding*(l.getHeight()));
				}
			}
			else if(g.getEdgeCount() <= 3*g.getNodeCount() - 6){	// Euler-Theorem: can be planar
				
				System.out.println("try planarize");
				
				/**
				 * Algorithm of Demoucron, Malgrange and Pertuiset
				 */
				/*if(!dmp(l, graphFactory)){
					
					System.out.println("planarize not possible");
					
					CycleLayouter.layout(	g,
											l.getMinX()+padding*(l.getWidth()), 
											l.getMinY()+padding*(l.getHeight()), 
											l.getWidth()-padding*(l.getWidth()), 
											l.getHeight()-padding*(l.getHeight()));
				}*/
				
				//as the above doesn't work yet, fall back to cycle layout
				CycleLayouter.layout(g,
									l.getMinX()+padding*(l.getWidth()), 
									l.getMinY()+padding*(l.getHeight()), 
									l.getWidth()-padding*(l.getWidth()), 
									l.getHeight()-padding*(l.getHeight()));
				
			}
			else{
				
				//GridLayouter.layout(g, l.getMinX()+padding*(l.getWidth()), l.getMinY()+padding*(l.getHeight()), l.getWidth()-padding*(l.getWidth()), l.getHeight()-padding*(l.getHeight()));
				CycleLayouter.layout(g, l.getMinX()+padding*(l.getWidth()), l.getMinY()+padding*(l.getHeight()), l.getWidth()-padding*(l.getWidth()), l.getHeight()-padding*(l.getHeight()));
				
			}
			
			/**
			 * Make sure coordinates are adopted by original graph on all implementations
			 */
			for(ConcurrentCoordinateNode n : g.getNodes()){
				graph.getNode(n.getIdentity()).setCoordinates(n.getCoordinates());
			}
			graph.repaint();
		}
	}
	
	/**
	 * Algorithm of Demoucron, Malgrange and Pertuiset
	 */
	public static boolean dmp(LayoutSplitNode l, GraphFactory graphFactory){
		
		//convenience variables
		BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge> g = l.getGraphComponent();
		
		//h <- any cycle of g	-preferably a long one
		Vector<ConcurrentCoordinateNode> h = null;
		for(ConcurrentCoordinateNode n : g.getNodes()){
			System.out.println("Startnode: " + n.getIdentity().intValue());
			Vector<ConcurrentCoordinateNode> cycle = dfs(g, n, n, null, new Vector<ConcurrentCoordinateNode>());
			if(cycle != null){
				System.out.print("Found cycle: ");
				for(ConcurrentCoordinateNode node : cycle){
					System.out.print(node.getIdentity().intValue() + ", ");
				}
				System.out.println("");
				h = cycle;
				break;
			}
		}
		if(h == null){
			System.out.println("break 1: " + g.getNodeCount());
			return false; // shouldn't happen
		}
		
		//find layout for cycle (and use a proportional size)
		double ratio = Math.sqrt((double)h.size() /  g.getNodeCount());
		CycleLayouter.layout(h, 
							l.getMinX()+l.getWidth()/2-ratio*l.getWidth()/2, 
							l.getMinY()+l.getHeight()/2-ratio*l.getHeight()/2, 
							l.getWidth()*ratio, 
							l.getHeight()*ratio);
		
		Set<ConcurrentCoordinateNode> outerArea = new HashSet<ConcurrentCoordinateNode>();
		Set<ConcurrentCoordinateNode> innerArea = new HashSet<ConcurrentCoordinateNode>();
		for(ConcurrentCoordinateNode n : h){
			outerArea.add(n);
			innerArea.add(n);
		}
		
		Vector<Set<ConcurrentCoordinateNode>> areas = new Vector<Set<ConcurrentCoordinateNode>>();
		Vector<Set<ConcurrentCoordinateNode>> fragments = new Vector<Set<ConcurrentCoordinateNode>>();
		areas.add(outerArea);
		areas.add(innerArea);
		
		for(Set<ConcurrentCoordinateNode> area : areas){
			System.out.print("Area: ");
			for(ConcurrentCoordinateNode n : area){
				System.out.print(n.getIdentity().intValue() + ", ");
			}
			System.out.println("");
		}
		
		//loop starts here
		
		//compute fragments
		Set<ConcurrentCoordinateNode> nodesInFragment = new HashSet<ConcurrentCoordinateNode>();
		for(ConcurrentCoordinateNode n : g.getNodes()){
			if(!h.contains(n) && !nodesInFragment.contains(n)){
				Set<ConcurrentCoordinateNode> fragment = new HashSet<ConcurrentCoordinateNode>();
				LinkedList<ConcurrentCoordinateNode> queue = new LinkedList<ConcurrentCoordinateNode>();
				
				fragment.add(n);				
				nodesInFragment.add(n);
				queue.addLast(n);
				
				while(!queue.isEmpty()){
					for(ConcurrentCoordinateNode n2 : g.adjacentTo(queue.remove())){
						fragment.add(n2);
						if(!h.contains(n2) && !nodesInFragment.contains(n2)){
							nodesInFragment.add(n2);
							queue.add(n2);
						}
					}
				}
				
				fragments.add(fragment);
			}
		}
		
		//compute valid areas
		Vector<Set<Set<ConcurrentCoordinateNode>>> validAreas = new Vector<Set<Set<ConcurrentCoordinateNode>>>();
		for(int i=0; i < fragments.size();++i){
			validAreas.add(new HashSet<Set<ConcurrentCoordinateNode>>());
		}
		for(Set<ConcurrentCoordinateNode> fragment : fragments){
			Set<ConcurrentCoordinateNode> contactNodes = new HashSet<ConcurrentCoordinateNode>();
			for(ConcurrentCoordinateNode n : fragment){
				if(h.contains(n)){
					contactNodes.add(n);
				}
			}
			
			for(Set<ConcurrentCoordinateNode> area : areas){
				boolean allContactNodesInArea = true;
				for(ConcurrentCoordinateNode n : contactNodes){
					if(!area.contains(n)) allContactNodesInArea = false;
				}
				if(allContactNodesInArea){
					System.out.println("valid area found");
					validAreas.elementAt(fragments.indexOf(fragment)).add(area);
				}
			}
			System.out.println("Valid areas: " + validAreas.elementAt(fragments.indexOf(fragment)).size());
		}
		
		for(Set<ConcurrentCoordinateNode> fragment : fragments){
			System.out.print("Fragment: ");
			for(ConcurrentCoordinateNode n : fragment){
				System.out.print(n.getIdentity().intValue() + ", ");
			}
			System.out.println("");
			
			for(Set<ConcurrentCoordinateNode> validArea : validAreas.get(fragments.indexOf(fragment))){
				System.out.print("Valid area: ");
				for(ConcurrentCoordinateNode n : validArea){
					System.out.print(n.getIdentity().intValue() + ", ");
				}
				System.out.println("");
			}
		}
		
		
		//step 3-6
		for(Set<Set<ConcurrentCoordinateNode>> validArea : validAreas){
			if(validArea.isEmpty()){
				System.out.println("break 2");
				return false;
			}
			else if(validArea.size() == 1){
				Vector<ConcurrentCoordinateNode> alphaPath = getAlphaPath(fragments.get(validAreas.indexOf(validArea)), h, g, graphFactory);
				Set<ConcurrentCoordinateNode> area = validArea.iterator().next();
				
				System.out.println("");
				
				if(alphaPath.size() < 3){
					System.out.println("Wrong alphaPath!!!");
					return false;
				}
				
				ConcurrentCoordinateNode startNode = alphaPath.remove(0);
				ConcurrentCoordinateNode endNode = alphaPath.remove(alphaPath.indexOf(alphaPath.lastElement()));
				
				int count = alphaPath.size();
				Point2D.Double coor1 = startNode.getCoordinates();
				Point2D.Double coor2 = endNode.getCoordinates();
				double width = coor1.x - coor2.x;
				double height = coor1.y - coor2.y;
				int i = 1;
				for(ConcurrentCoordinateNode n : alphaPath){
					n.setCoordinates(coor1.x + i * width/(count+1), coor1.y + i * height/(count+1));
					i++;
				}
				
				h.addAll(alphaPath);
			}
			else{
				
			}
		}
		
		
		//return true;
		System.out.println("break 3");
		
		return true;
	}
	
	private static Vector<ConcurrentCoordinateNode> getAlphaPath(	Set<ConcurrentCoordinateNode> fragment, 
														Vector<ConcurrentCoordinateNode> h, 
														BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge> graph,
														GraphFactory graphFactory){
		
		ConcurrentCoordinateNode startNode = null;
		ConcurrentCoordinateNode endNode = null;
		
		for(ConcurrentCoordinateNode n : fragment){
			if(h.contains(n) && startNode == null) startNode = n;
			else if(h.contains(n)){
				endNode = n;
				break;
			}
		}
		Set<ConcurrentCoordinateNode> notContactNodes = new HashSet<ConcurrentCoordinateNode>();
		for(ConcurrentCoordinateNode n : fragment){
			if(!h.contains(n)) notContactNodes.add(n);
		}
		notContactNodes.add(startNode);
		notContactNodes.add(endNode);
		
		BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge> graph2 = graph.getInducedSubgraph(notContactNodes, graphFactory);
		
		return bfs(graph2, startNode, endNode, graphFactory);
	}
	
	private static Vector<ConcurrentCoordinateNode> bfs(BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge> graph, 
														ConcurrentCoordinateNode startNode, 
														ConcurrentCoordinateNode endNode, 
														GraphFactory graphFactory){
		Vector<ConcurrentCoordinateNode> result = new Vector<ConcurrentCoordinateNode>();
		Map<ConcurrentCoordinateNode, ConcurrentCoordinateNode> parent = new HashMap<ConcurrentCoordinateNode, ConcurrentCoordinateNode>();
		LinkedList<ConcurrentCoordinateNode> queue = new LinkedList<ConcurrentCoordinateNode>();
		queue.add(startNode);
		
		while(!queue.isEmpty()){
			ConcurrentCoordinateNode n = queue.remove();
			Set<ConcurrentCoordinateNode> set = graph.adjacentTo(n);
			for(ConcurrentCoordinateNode n2 : set){
				if(n2 == endNode){
					result.add(endNode);
					ConcurrentCoordinateNode n3 = endNode;
					while(parent.get(n3) != null){
						n3 = parent.get(n3);
						result.add(0, n3);
					}
					return result;
				}
				parent.put(n2, n);
				queue.add(n2);
			}
		}
		return null;				
	}
	
	/**
	 * Depth first search with backtracking
	 * Used to find a cycle. Return a set if current node is part of a cycle (undirected, so always)
	 * @param graph
	 * @param startNode
	 * @param currentNode
	 * @param visited
	 * @return
	 */
	private static Vector<ConcurrentCoordinateNode> dfs(BaseGraph<ConcurrentCoordinateNode, ConcurrentCoordinateEdge> graph, 
														ConcurrentCoordinateNode startNode, 
														ConcurrentCoordinateNode currentNode, 
														ConcurrentCoordinateNode parentNode, 
														Vector<ConcurrentCoordinateNode> visited){
		
		if(visited.contains(currentNode)){
			if(startNode.equals(currentNode)){
				return visited;
			}
			return null;
		}
		visited.add(currentNode);
		for(ConcurrentCoordinateNode n : graph.adjacentTo(currentNode)){
			if(n == parentNode) continue;
			Vector<ConcurrentCoordinateNode> res = dfs(graph,startNode,n, currentNode, visited);
			if(res != null)
				return res;
		}
		visited.remove(currentNode);
		return null;
	}
}
