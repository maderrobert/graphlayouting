package layout;

import java.awt.geom.Point2D;

import guiGraph.ConcurrentCoordinateNode;
import guiGraph.ConcurrentGraphWrapper;

/**
 * 
 * @author Robert Mader
 *
 */
public class PublicLayoutFuntions {
	
	private static double defaultSize = 400;
	
	private static class Combo {
	   BarnesHutNode node;
	   int counter;
	}

	public static void addCoordinatesToNode(ConcurrentGraphWrapper graph, ConcurrentCoordinateNode node){
		
		//calc min/max for area
		
		double minX = Double.POSITIVE_INFINITY;
		double minY = Double.POSITIVE_INFINITY;
		double maxX = Double.NEGATIVE_INFINITY;
		double maxY = Double.NEGATIVE_INFINITY;
		
		for(ConcurrentCoordinateNode n : graph.getNodes()){
			if(n.getCoordinates().x < minX)
				minX = n.getCoordinates().x;
			if(n.getCoordinates().x > maxX)
				maxX = n.getCoordinates().x;
			if(n.getCoordinates().y < minY)
				minY = n.getCoordinates().y;
			if(n.getCoordinates().y > maxY)
				maxY = n.getCoordinates().y;
		}
		
		
		//prevent bugs for whatever reason
		if(			minX == Double.POSITIVE_INFINITY 
				|| 	minY == Double.POSITIVE_INFINITY
				|| 	maxX == Double.NEGATIVE_INFINITY 
				|| 	maxY == Double.NEGATIVE_INFINITY){
			minX = 0;
			minY = 0;
			maxX = defaultSize;
			maxY = defaultSize;
		}
		
		//make min size of fields - to be removed when painter rescales properly
		if(maxX - minX < defaultSize){
			minX -= (defaultSize - (maxX - minX))/2;
			maxX += (defaultSize - (maxX - minX))/2;
		}
		if(maxY - minY < defaultSize){
			minY -= (defaultSize - (maxY - minY))/2;
			maxY += (defaultSize - (maxY - minY))/2;
		}
		
		//normalize to 0 - to be removed when painter rescales properly
		if(minX != 0){
			maxX -= minX;
			minX = 0;
		}
		if(minY != 0){
			maxY -= minY;
			minY = 0;
		}
		
		
		//create tree
		BarnesHutNode bhTree = new BarnesHutNode(graph, new Point2D.Double(minX,minY), Double.max(maxX-minX, maxY-minY));
		for(ConcurrentCoordinateNode n : graph.getNodes()){
			bhTree.addNode(n);
		}
		
		Combo leaf = getNearestLeaf(bhTree, 0);
		
		if(leaf.node.isEmpty()){
			node.setCoordinates(leaf.node.getCenter());
		}
		else{
			if(leaf.node.getCenterOfMass().x < leaf.node.getCenter().x){
				node.setCoordinates(new Point2D.Double(leaf.node.getCenter().x + leaf.node.getSize()/4, leaf.node.getCenter().y));
			}
			else{
				node.setCoordinates(new Point2D.Double(leaf.node.getCenter().x - leaf.node.getSize()/4, leaf.node.getCenter().y));
			}
		}
	}
	
	private static Combo getNearestLeaf(BarnesHutNode node, int counter){
		if(node.isExternal()){
			Combo result = new Combo();
			result.node = node;
			result.counter = counter;
			return result;
		}
		else{
			Combo result = new Combo();
			result.node = null;
			result.counter = Integer.MAX_VALUE;
			for(BarnesHutNode n : node.getChilds()){
				Combo leaf = getNearestLeaf(n, counter+1);
				if(leaf.counter < result.counter){
					result.node = leaf.node;
					result.counter = leaf.counter;
				}
			}
			return result;
		}
	}
}
